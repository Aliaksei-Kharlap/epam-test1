
import requests
from bs4 import BeautifulSoup
import csv
from datetime import datetime
import sqlite3


def reply():
    DOLLAR_BYN = "https://en.wikipedia.org/wiki/Special:Random"
    headers = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"}

    full_page = requests.get(DOLLAR_BYN, headers=headers)
    dad = BeautifulSoup(full_page.content, "html.parser")
    name_page = dad.find("h1", )
    name_title = name_page.text
    link = dad.find("link", rel="canonical")
    link2 = link.get("href")
    converts = dad.find_all("a", )
    one_page = []
    for convert in converts:
        one_page.append(convert.get("href"))

    answer = []
    for one in one_page:
        if one and "#" not in one:
            answer.append(one)

    with open('example3.csv', 'a', newline="") as csvfile:
        fieldnames = ['Date', 'Time', 'Name', "Links", "Link"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        date = datetime.now().date()
        time = datetime.now().time()
        writer.writerow({'Date': date, 'Time': time, 'Name': name_title, "Links": answer, "Link": link2})
    db.insert_date(name=name_title, date=date, time=time, links=answer, link=link2)

    print(answer)


def get_links_by_name( name):
    print(db.get_links(name=str(name)))


def get_number_by_name(name):
    print(db.get_number(name=str(name)))


def get_num_of_links_in_DB(name):
    print(db.get_num_of_links_in_DB(name=str(name)))

class DB:
    def __init__(self):
        self.conn = sqlite3.connect("article.db")
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS article (name text, date text, time text, links text, link text)'''
        )
        self.conn.commit()

    def insert_date(self, name, date, time, links, link):
        res = []
        for row in self.c.execute("SELECT links FROM article WHERE name = (?)", (str(name),)):
            res.append(row)
        if res:
            return res
        else:
            self.c.execute("INSERT INTO article VALUES (?, ?, ?, ?, ?)", (name, str(date), str(time), ", ".join(links), link))
            self.conn.commit()

    def get_links(self, name):
        res = []
        for row in self.c.execute("SELECT links FROM article WHERE name = (?)", (str(name),)):
            res.append(row)
        if res:
            res2 = res[0][0].split(", ")
            return res2
        else:
            return "This page does not found"

    def get_number(self, name):
        res = []
        for row in self.c.execute("SELECT links FROM article WHERE name = (?)", (str(name),)):
            res.append(row)
        if res:
            res2 = res[0][0].split(", ")
            return len(res2)
        else:
            return "This page does not found"

    def get_num_of_links_in_DB(self, name):
        res = []
        for row in self.c.execute("SELECT links FROM article"):
            res.append(row)
            res2 = []
        for i in res:
            for ii in i:
                res2.append(ii)
        res3 = []
        for row in self.c.execute("SELECT link FROM article WHERE name = (?)", (str(name),)):
            res3.append(row)

        if res3:
            num = 0
            for i in res2:
                if i == res3[0][0][24:]:
                    num += 1
            return num
        else:
            return "This page does not found"

db = DB()

ans = True
while ans:
    answer = input("Выберите действие (введите число):\n 1 - Получить список ссылок на рандомной статье в википедии\n 2 - Получить список сылок статьи википедии по её названию\n 3 - Получить количество ссылок в статье по её названию\n 4 - Получить количество статьи в БД по её названию\n 5 - Выход\n")
    if answer == "1":
        reply()
    elif answer == "2":
        name = input("Введите название статьи:")
        get_links_by_name(name)
    elif answer == "3":
        name = input("Введите название статьи:")
        get_number_by_name(name)
    elif answer == "4":
        name = input("Введите название статьи:")
        get_num_of_links_in_DB(name)
    elif answer == "5":
        ans = False
    else:
        continue
print("Выход выполнен")







if __name__ == "__main__":
    db = DB()


