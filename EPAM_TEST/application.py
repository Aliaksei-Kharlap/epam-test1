
import tkinter as tk
from tkinter.messagebox import showinfo
import requests
from bs4 import BeautifulSoup
import csv
from datetime import datetime
import sqlite3

class MyGui(tk.Frame):
    def __init__(self, parent=None):
        tk.Frame.__init__(self, parent)
        button = tk.Button(self, text="Get links on random page", command=self.reply)
        button.pack()
        tk.Label(root, text='Enter the article name to get links from DB:').pack()
        ent = tk.Entry(root)
        ent.pack()
        btn = tk.Button(root, text="Get list of links by article name", command = (lambda: self.get_links_by_name(ent.get())))
        btn.pack()

        tk.Label(root, text='Enter the article name to get number of links on this page:').pack()
        enty = tk.Entry(root)
        enty.pack()
        bton = tk.Button(root, text="Get number of links by article name",
                        command=(lambda: self.get_number_by_name(enty.get())))
        bton.pack()

        tk.Label(root, text='Enter the article name to get number of links to this page in DB:').pack()
        entyy = tk.Entry(root)
        entyy.pack()
        btonn = tk.Button(root, text="Get number of links to the page in DB by article name",
                        command=(lambda: self.get_num_of_links_in_DB(entyy.get())))
        btonn.pack()

        self.db = db

    def reply(self):
        DOLLAR_BYN = "https://en.wikipedia.org/wiki/Special:Random"
        headers = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"}

        full_page = requests.get(DOLLAR_BYN, headers=headers)
        dad = BeautifulSoup(full_page.content, "html.parser")
        name_page = dad.find("h1",)
        name_title = name_page.text
        link = dad.find("link", rel="canonical")
        link2 = link.get("href")
        converts = dad.find_all("a", )
        one_page = []
        for convert in converts:
            one_page.append(convert.get("href"))

        answer = []
        for one in one_page:
            if one and "#" not in one:
                answer.append(one)

        with open('example3.csv', 'a', newline="") as csvfile:
            fieldnames = ['Date', 'Time', 'Name', "Links", "Link"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            date = datetime.now().date()
            time = datetime.now().time()
            writer.writerow({'Date': date, 'Time': time, 'Name': name_title, "Links":answer, "Link":link2})
        db.insert_date(name=name_title, date=date, time=time, links=answer, link=link2)


        showinfo(title="popup", message=answer)


    def get_links_by_name(self, name):
        showinfo(title="popup", message=db.get_links(name=str(name)))

    def get_number_by_name(self, name):
        showinfo(title="popup", message=db.get_number(name=str(name)))

    def get_num_of_links_in_DB(self, name):
        showinfo(title="popup", message=db.get_num_of_links_in_DB(name=str(name)))



class DB:
    def __init__(self):
        self.conn = sqlite3.connect("article.db")
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS article (name text, date text, time text, links text, link text)'''
        )
        self.conn.commit()

    def insert_date(self, name, date, time, links, link):
        res = []
        for row in self.c.execute("SELECT links FROM article WHERE name = (?)", (str(name),)):
            res.append(row)
        if res:
            return res
        else:
            self.c.execute("INSERT INTO article VALUES (?, ?, ?, ?, ?)", (name, str(date), str(time), ", ".join(links), link))
            self.conn.commit()

    def get_links(self, name):
        res = []
        for row in self.c.execute("SELECT links FROM article WHERE name = (?)", (str(name),)):
            res.append(row)
        if res:
            res2 = res[0][0].split(", ")
            return res2
        else:
            return "This page does not found"

    def get_number(self, name):
        res = []
        for row in self.c.execute("SELECT links FROM article WHERE name = (?)", (str(name),)):
            res.append(row)
        if res:
            res2 = res[0][0].split(", ")
            return len(res2)
        else:
            return "This page does not found"

    def get_num_of_links_in_DB(self, name):
        res = []
        for row in self.c.execute("SELECT links FROM article"):
            res.append(row)
            res2 = []
        for i in res:
            for ii in i:
                res2.append(ii)
        res3 = []
        for row in self.c.execute("SELECT link FROM article WHERE name = (?)", (str(name),)):
            res3.append(row)

        if res3:
            num = 0
            for i in res2:
                if i == res3[0][0][24:]:
                    num += 1
            return num
        else:
            return "This page does not found"

if __name__ == "__main__":
    db = DB()
    root = tk.Tk()
    window = MyGui()
    window.pack()
    root.title("Parsing_pages")
    root.geometry("650x450+300+200")
    root.resizable(False, False)
    root.mainloop()